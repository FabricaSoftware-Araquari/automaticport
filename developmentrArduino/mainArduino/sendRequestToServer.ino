void sendRequestToServer(String codeRFID){
  /*
    Função recebe um código RFID e realiza requisição ao web service para solicitar acesso ao ambiente
    Se o usuário estiver liberado retorna a (response', True), caso contrário (response', False)
  */
  
 
  
  //Inicia Cliente Web
  EthernetClient clientArduino;
  String response;  
  int count = 0;  

  //Conecta no servidor web e realiza um request via GET
  if (clientArduino.connect(server, 8080)) {
    clientArduino.println("GET /automaticport/requestarduino/"+codeRFID+" HTTP/1.1");
    clientArduino.println("Connection: close");
    clientArduino.println();
    
  } else {}

  //Aguarda retorno do servidor, com o time out máximo de 1000 voltas no while.
  while (!clientArduino.available() && count < 8000){count++;}
  
  //Realiza a leitura do retorno do servidor web
  while (clientArduino.available()) {
    char c = clientArduino.read();
    response += c;
  }

  //Interrompe conexão com servidor
  clientArduino.stop();
  
 //Chama a abertura da porta, independente do resultado do servidor
  if (response.indexOf("True") > 0 ){
    managerArduinoDoor();
  } else {
    aguarde();
  }
  
}
