void setup(){
    // Inicializando a conexão Serial com Hardware
      Serial.begin(9600);
      while (!Serial) {;}

    //Inicializando a biblioteca SPI para controle de interupções
    SPI.begin();

    //Inicializando o controle do Cartão RFID
    mfrc522.PCD_Init(); 

    //Iniciar conexão de rede com Arduíno
    Ethernet.begin(mac, ip);
    serverArduino.begin();
    Serial.print("server is at ");
    Serial.println(Ethernet.localIP());

    //Inicializando a configuração de controle dos pinos
    pinMode(pinDoorGreen, OUTPUT);
    pinMode(pinDoorRed, OUTPUT);
    pinMode(pinDoorYellow, OUTPUT);

    
    //pinMode(pinOpenCloseDoor, OUTPUT);

    //Escrevendo  o valor default
    digitalWrite(pinDoorRed, HIGH);
    digitalWrite(pinOpenCloseDoor, HIGH);

       
}


