void managerArduinoDoor(){
  abrir_Porta();
  delay(3500);
  fechar_Porta();

}


void fechar_Porta(){
  Serial.println("Porta Fechada");
  digitalWrite(pinDoorGreen, LOW);
  digitalWrite(pinDoorRed, HIGH);
  digitalWrite(pinOpenCloseDoor, HIGH);


}

void abrir_Porta(){
  Serial.println("Porta Aberta");
  digitalWrite(pinDoorRed, LOW);
  digitalWrite(pinDoorYellow, HIGH);
  delay(2500);
  digitalWrite(pinDoorYellow, LOW);
  digitalWrite(pinDoorGreen, HIGH);
  digitalWrite(pinDoorRed, LOW);
  digitalWrite(pinOpenCloseDoor, LOW);
    


}

void aguarde(){
  Serial.println("Aguarde");
  digitalWrite(pinDoorRed, LOW);
  digitalWrite(pinDoorYellow, HIGH);
  delay(2500);
  digitalWrite(pinDoorYellow, LOW);
  digitalWrite(pinDoorRed, HIGH);
}
