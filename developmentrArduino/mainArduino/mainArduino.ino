#include <Ethernet.h>
#include <SPI.h>
#include <MFRC522.h>
#define SS_PIN 53  
#define RST_PIN 5.

//Configuração das portas de Controle do Leitor de Cartão
MFRC522 mfrc522(SS_PIN, RST_PIN); 

//Configuração da interface de rede do Arduíno

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 0, 125);
IPAddress server(192, 168, 0, 3);

//Configuração do Cliente/Servidor Arduíno 
EthernetServer serverArduino(8080);


//Configuração dos Pinos de Led Red = Porta fechada, Green = Porta Aberta
int pinDoorGreen = 13;
int pinDoorRed = 12;
int pinDoorYellow = 11;
int pinOpenCloseDoor = 10;

