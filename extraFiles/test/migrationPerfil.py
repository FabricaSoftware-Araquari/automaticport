#coding:utf-8

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'internetOfThings.settings')

import django
django.setup()

from automaticPort.models.perfil import Perfil


def createPerfis():
    listaPerfis = ["usuario", "administrador"]
    for perfil in listaPerfis:
        if perfil not in (Perfil.objects.all()):
            Perfil.objects.create(nome=perfil).save()


if __name__ == '__main__':
    print ("Inicio da Migração")
    createPerfis()
