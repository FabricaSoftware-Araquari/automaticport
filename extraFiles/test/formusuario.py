#coding:utf-8
from django import forms
from django.contrib.auth.models import User

class FormUsuario(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))

    def save(self, commit=True):
        usuario = super(FormUsuario, self).save(commit=False)
        usuario.set_password(self.cleaned_data['password'])
        if commit:
            usuario.save()
        return usuario

    class Meta:
        model = User
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Usuario'}),
            'first_name': forms.TextInput(attrs={'placeholder': 'Nome'}),
            'password': forms.TextInput(attrs={'placeholder': 'Nome'}),
            'email': forms.TextInput(attrs={'placeholder': 'E-Mail'}),
        }
        fields = ('first_name','username', 'email', 'password', )