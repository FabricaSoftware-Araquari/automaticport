#coding:utf-8

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def emailAtivacaoCadastro(nomeUsuario, emailUsuario, assunto, chaveAtivacao, dominio):
        me = "contato@sisca.fabricadesoftware.ifc.edu.br"
        you = "{}".format(emailUsuario)
        subject = assunto

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = me
        msg['To'] = you

        html = """
                    <h2><b>Ola  {}.</b></h2>
                    <h3>Seja Bem Vindo ao <strong>SisCA</strong> - Sistema de Controle de Acesso</h3>
                    <p>Para Confirmar Seu cadastro, Clique no Link Abaixo<br>
                        <a href="http://{}/automaticport/ativacadastro/{}">Ativar Cadastro</a>
                    </p>

                    <p>Atenciosamente,
                    <br>
                    Equipe de Suporte<br>
                    giorgyismael@gmail.com<br>
                    +55 (47)84390048 | (47) 84390048</p>
                    """.format(nomeUsuario.split(" ")[0], dominio, chaveAtivacao)

        txt = MIMEText(html, 'html')
        msg.attach(txt)
        s = smtplib.SMTP('localhost')
        s.sendmail(me, you, msg.as_string())
        s.quit()

def emailRecuperarSenha(nomeUsuario, emailUsuario, assunto, chaveAtivacao, dominio):
        me = "contato@sisca.fabricadesoftware.ifc.edu.br"
        you = "{}".format(emailUsuario)
        subject = assunto

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = me
        msg['To'] = you

        html = """
                    <h2><b>Ola  {}.</b></h2>
                    <p>Para redefinir sua senha, clique no link abaixo<br>
                        <a href="http://{}/automaticport/recuperarsenhachave/{}">Redefinir Senha</a>
                    </p>

                    <p>Atenciosamente,
                    <br>
                    Equipe de Suporte<br>
                    giorgyismael@gmail.com<br>
                    +55 (47)84390048 | (47) 84390048</p>
                    """.format(nomeUsuario.split(" ")[0], dominio,chaveAtivacao)

        txt = MIMEText(html, 'html')
        msg.attach(txt)
        s = smtplib.SMTP('localhost')
        s.sendmail(me, you, msg.as_string())
        s.quit()


def emailFaleConosco(nomeUsuario, emailUsuario, assunto, descricao):
        me = "faleconosco@sisca.fabricadesoftware.ifc.edu.br"
        you = "{}".format(emailUsuario)
        subject = assunto

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = me
        msg['To'] = you

        html = """
                    <h2><b>Olá. </b></h2>
                    <p>Comunicamos o recebimento do fale conosco abaixo, pedimos a gentileza que aguarde contato.</p>
                    <p>Nome:{}<br>
                    Email:{}<br>
                    Descrição:{}</p>

                    <p>Atenciosamente,
                    <br>
                    Equipe de Suporte<br>
                    giorgyismael@gmail.com<br>
                    +55 (47)84390048 | (47) 84390048</p>
                    """.format(nomeUsuario,emailUsuario,descricao)

        txt = MIMEText(html, 'html')
        msg.attach(txt)
        s = smtplib.SMTP('localhost')
        s.sendmail(me, you, msg.as_string())
        s.quit()